/*
 armoryBot.js

 Version 0.4

 Copyright (c) 2016 Lucas Gramajo

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */


var token = "YOUR_BOTS_TOKEN"; // Get a token from Discord for your bot. https://discordapp.com/developers/applications/me
var bungieApiKey = "YOUR_BUNGIE_KEY"; // <- This you need to get from Bungie: https://www.bungie.net/en/user/api
var xboxLive = "1";
var ps4="2";
var platform = ps4; // Change to xboxLive if you need to.

// ************************************

var request = require('request');
var DiscordClient = require('discord.io');
var bot = new DiscordClient({
    autorun: true,
    token : token
});

// Get guns
var mainURL = "https://www.bungie.net/Platform/Destiny/";

// IDs for each armory item type
var armoryItemType = {
    "heavy" : "953998645",
    "special" : "2465295065",
    "secondary" : "2465295065",
    "primary" : "1498876634",
    "chest" : "14239492",
    "gauntlet" : "3551918588",
    "glove" : "3551918588",
    "gloves" : "3551918588",
    "arms" : "3551918588",
    "gauntlets" : "3551918588",
    "classItem" : "1585787867",
    "helmet" : "3448274439",
    "artifact" : "434908299",
    "ghost" : "4023194814",
    "legs" : "20886954",
    "boots" : "20886954"
};

var damageStepHashes = {
    voidDamage : "472357138",
    kinetic : "643689081"
};

var charTypes = {
    "0" : "Titan",
    "1" : "Hunter",
    "2" : "Warlock"
};

// SEARCH USER
var headers = {
    'X-API-Key': bungieApiKey
};

var ArmoryRequest = function(){

    var channelIdGlobal;
    var self;
    var executeAfterAllRequests;

    // Init variables and objects
    var parameters = {
        user : "",
        armoryItemType : "primary",
        characterTypeId : 0,
        characterTypeName : charTypes[0],
        membershipId : "",
        itemId : ""
    };

    this.init = function(user, userID, channelID, messageArr, callback){

        self = this;
        executeAfterAllRequests = callback;

        // Set defaults
        channelIdGlobal = channelID;

        if (self.setUpSearchParams(messageArr, user) ){
            self.searchUser()
        }

    };

    // Breaks up all parameters send in message and stores them in instance variable for this object.
    // Returns true or false it's successful at finding parameters
    this.setUpSearchParams = function(messageArr, user){

        // Check we got at least one command, otherwise it's an error.
        // Return false to stop the search if true.
        if (messageArr.length == 1){
            self.displayError();
            return false;
        }

        // Check if we have a user ID or not.
        if (messageArr.length == 2){
            parameters.armoryItemType = messageArr[1];
            parameters.user = user;

            // Check if we have multiple parameters
        } else if (messageArr.length > 2) {

            var secondCommand = messageArr[2];
            var firstCommand = messageArr[1];

            // Check if second command is a number
            if (Number.isFinite(Number(secondCommand))){

                var secondCommandAsCharType = Number(secondCommand);
                parameters.armoryItemType = firstCommand;
                parameters.characterTypeId = self.getValidatedCharacterId(secondCommandAsCharType);
                parameters.user = user;

                // Check if third command is the character number
            }else if(messageArr.length >= 4){

                parameters.user = firstCommand;
                parameters.armoryItemType = secondCommand;
                var thirdCommand = messageArr[3];

                // Check if third command (character number) is indeed a number
                if(Number.isFinite( Number(thirdCommand) ) ){
                    var thirdCommandAsCharType = Number(thirdCommand);
                    parameters.characterTypeId = self.getValidatedCharacterId(thirdCommandAsCharType);

                }else{
                    // Looks like no character number, leave as default
                }

                // Looks like no character number, leave as default
            }else{
                //console.log("Something was not correct, so resetting to defaults. Just weapon and username. "+thirdCommand);
                parameters.user = firstCommand;
                parameters.armoryItemType = secondCommand;
            }

        }

        // Verify weapon type is correct. Return false to stop the search.
        if(self.armoryTypeItemIsInvalid(parameters.armoryItemType)){
            //console.log("Weapon type is invalid. " + parameters.armoryItemType);
            self.displayError();
            return false;
        }

        return true;

    };

    // allStatDefinitions and itemDefinitions are to set of stats found on the bungie definitions object. Why there's
    // two, we don't know, but one has the name (allStatDefinitions), and the other one the values (itemDefinitions).
    this.getStats = function(allStatDefinitions, itemStats, itemDefinitions){

        // Standard stats
        var itemStatDefinitions = itemDefinitions.stats;
        var itemStatsDefinitionArray = [];

        // These definitions have little info (like no name), but have values.
        var statsDef;

        // Odd bungie data. There's these definition stats that have a lot of info, but no values.
        var statsDefWithNames;

        // A single stat for an item, this is not a definition. This is the actual value for the equipped gun.
        var stat;

        // Equipped stats based on perks
        var equippedStats = [];

        for (var k = 0; k < itemStats.length; k++){

            stat = itemStats[k];

            if (allStatDefinitions.hasOwnProperty(stat.statHash)) {

                statsDef = itemDefinitions.stats[stat.statHash];
                statsDefWithNames = allStatDefinitions[stat.statHash];

                // If we have stats, go ahead.
                if (typeof statsDef != 'undefined'){

                    // Compare definition value with item value
                    if (typeof statsDef.value == 'undefined' || stat.value == 'undefined') {
                        self.displayErrorWithMsg("Defense stats are missing a value.");
                        break;
                    }

                    if (statsDef.value != stat.value){
                        // Push to equipped stats array
                        equippedStats.push(statsDefWithNames.statName + " : " + stat.value +" ( "+statsDef.value+" )");
                    }else{
                        // Push to equipped stats array
                        equippedStats.push(statsDefWithNames.statName + " : " + stat.value);
                    }


                    // Delete from secondary group
                    delete itemStatDefinitions[stat.statHash];
                }

            }

        }

        // Standard Stats
        for (var itemStatKey in itemStatDefinitions) {
            if (itemStatDefinitions.hasOwnProperty(itemStatKey)) {
                stat = itemStatDefinitions[itemStatKey];
                statsDefWithNames = allStatDefinitions[itemStatKey];
                itemStatsDefinitionArray.push( statsDefWithNames.statName+" : "+stat.value );
            }
        }

        return "__Equipped Stats :__ "+equippedStats.join(" | ")+" | "+itemStatsDefinitionArray.join(" | ");

    };

    this.getValidatedCharacterId = function(characterIdString) {

        var characterNum = Number(characterIdString);

        if (characterNum < 4 && characterNum > 0) {
            // People don't have to understand how arrays work, so for them
            // the first character will be one, second two, etc... So we need to
            // substract one to that number
            return characterNum - 1;
        }

        return 0;
    };

    this.searchUser = function(){

        var searchUserOptions = {
            url: mainURL+'SearchDestinyPlayer/'+platform+'/'+parameters.user,
            headers: headers
        };

        makeRequest(searchUserOptions, function(body){

            if(body.Response.length > 0){
                parameters.membershipId = body.Response[0].membershipId;
                self.getPlayerAccount();
            }else{
                //console.log(" User not found or service down: "+JSON.stringify(body));
                ArmoryBot.postMsg("Couldn't find that user.", channelIdGlobal)

            }
        })
    };

    this.displayError = function(){
        ArmoryBot.postMsg("Something went wrong. Here's how to use it: @gunsmith [optional]gamerTag primary.We'll try and match your Discord user id to a PSN tag if no tag is sent. Here are the commands: heavy, special, secondary, primary, chest, gauntlet, glove, arms, classItem, helmet, artifact, ghost, legs, boot.", channelIdGlobal)
    };

    this.displayErrorWithMsg = function(message){
        ArmoryBot.postMsg(message, channelIdGlobal)
    };

    this.armoryTypeItemIsInvalid = function(){

        var wepType = parameters.armoryItemType;
        for (var weaponKey in armoryItemType){
            if (wepType == weaponKey){
                return false;
            }
        }

        return true;

    };

    this.getPlayerAccount = function(){

        var getUserOptions = {
            url: mainURL+platform+'/Account/'+ parameters.membershipId +'/Summary/',
            headers: headers
        };

        makeRequest(getUserOptions, function(body){

            var dataIsSafe = self.isDataSafe(body);
            if (!dataIsSafe) return;

            var characterBase = body.Response.data.characters[parameters.characterTypeId].characterBase;
            parameters.characterTypeName = charTypes[characterBase.classType];
            parameters.characterTypeId = characterBase.characterId;
            //console.log(parameters.characterTypeId);

            self.getCharacterInfo()

        })

    };

    this.isDataSafe = function(body){
        if(typeof body.Response == "undefined" || typeof body.Response.data == "undefined"){
            ArmoryBot.postMsg("Something's up with Bungie's server.",channelIdGlobal);
            return false;
        }

        return true;
    };

    this.getCharacterInfo = function(){

        var getCharacterOptions = {
            url: mainURL+platform+'/Account/'+ parameters.membershipId +'/Character/'+ parameters.characterTypeId +'/Inventory/Summary',
            headers: headers
        };

        makeRequest(getCharacterOptions, function(body){
            var dataIsSafe = self.isDataSafe(body);

            if (!dataIsSafe) return;

            var items = body.Response.data.items;
            var armoryTypeHash = armoryItemType[parameters.armoryItemType];

            for (var i = 0 ; i < items.length ; i++){

                var item = items[i];

                // transferStatus == 1 means that item is the equipped one. The service
                // returns all items, even the non equipped ones.
                var isEquippedItem = item.transferStatus == 1;

                // Check armory type (primary, heavy, etc...) match, and that the item is an equipped one.
                if ( item.bucketHash == armoryTypeHash && isEquippedItem){
                    parameters.itemId = item.itemId;
                    self.getItemInfo();
                }
            }

        })
    };

    this.getItemInfo = function(){

        var getItemsOptions = {
            url: mainURL+platform+'/Account/'+ parameters.membershipId+'/Character/'+parameters.characterTypeId+'/Inventory/'+ parameters.itemId+'?definitions=true',
            headers: headers
        };

        makeRequest(getItemsOptions, function(body){

            var dataIsSafe = self.isDataSafe(body);
            if (!dataIsSafe) return;

            var data = body.Response.data;
            var itemHash = data.item.itemHash;
            var light = "";

            // Bungie has a tendency of not sending this values with some gear.
            if (typeof data.item.primaryStat != 'undefined') {
                if (typeof data.item.primaryStat.value != 'undefined') {
                    light = data.item.primaryStat.value;
                }
            }

            var definitions = body.Response.definitions;
            var itemDefinitions = definitions.items[itemHash];
            var talentGrids = definitions.talentGrids;
            var allStatsDefinitions = definitions.stats;
            var damageElement = data.item.damageTypeHash;
            var damageTypeName = null;
            if(definitions.damageTypes == "undefined" || typeof definitions.damageTypes == "undefined"){
                if(definitions.damageTypes[damageElement] == "undefined" || typeof definitions.damageTypes[damageElement] == "undefined") {
                    damageTypeName = definitions.damageTypes[damageElement].damageTypeName;
                }
            }
            //var damageTypeName = definitions.damageTypes[damageElement].damageTypeName;

            // Construct string for bot to print
            var result = "**"+itemDefinitions.itemName+" ( "+light+" )** - "+itemDefinitions.itemTypeName+" *("+ parameters.characterTypeName+" - "+ parameters.user +")* \n";
            if(damageTypeName){
                result += damageTypeName+" Damage\n";
            }
            result +=  self.getPerks(data, talentGrids)+"\n"+ self.getStats(allStatsDefinitions, data.item.stats, itemDefinitions);

            // Set item icon.
            result +=  "\nhttps://www.bungie.net/" + itemDefinitions.icon;

            executeAfterAllRequests(result);

        })
    };

    this.getPerks = function(data, talentGrids){

        // This will iterate through the gun/armor talent nodes, and then try and find
        // the correct names from the talentGrids which is part of the definitions object.
        var talentNodes = data.talentNodes;
        var perkDefinitions = talentGrids[Object.keys(talentGrids)[0]].nodes;
        var perksObj = {};

        var getPerkName = function(talentNode){

            var perk = {"name" : "", "column" : "", "row" : ""},
                stepIndex = talentNode.stepIndex,
                nodeIndex = talentNode.nodeIndex,
                nodeNameStr = "";

            // Find name and then create perk string.
            for (var i = 0; i < perkDefinitions.length; i++){
                var perkDef = perkDefinitions[i];

                if(perkDef.nodeIndex == nodeIndex){

                    /* Check is not a damage type, which will be incorrect
                     For some reason the service is plagued with this damage types in
                     the perk nodes that are incorrect. We get the damage type from somewhere else.
                     For now we check for Void and Kinetic only.*/
                    var nodeStepHash = perkDef.steps[stepIndex].nodeStepHash;
                    if (nodeStepHash == damageStepHashes.voidDamage || nodeStepHash == damageStepHashes.kinetic ){
                        return false;
                    }

                    nodeNameStr = perkDef.steps[stepIndex].nodeStepName;

                    // Check if node is active or not. If it's not, use strike through on text.
                    if (talentNode.isActivated == false) {
                        nodeNameStr = "~~" + nodeNameStr + "~~";
                    }

                    break;
                }
            }

            // Check node name didn't return some error. If it did return false and it will be ignored.
            if(nodeNameStr == "" || typeof nodeNameStr == 'undefined'){
                return false;
            }

            perk.name = nodeNameStr;
            perk.column = perkDef.column;
            perk.row = perkDef.row;

            return perk;

        };

        for (var j = 0 ; j < talentNodes.length; j++){

            // Check invalid nodes
            if (talentNodes[j].stateId != "Invalid" && talentNodes[j].stateId != "Hidden"){

                var perk = getPerkName(talentNodes[j]);

                // If perk returns false, skip
                if(perk) {
                    var perkColumnStr = perk.column + "";

                    if (!perksObj.hasOwnProperty(perkColumnStr)) {
                        perksObj[perkColumnStr] = []
                    }

                    perksObj[perkColumnStr].splice(perk.row, 0, perk.name);
                }
            }
        }

        var perksStr = "";
        for(var perkColumn in perksObj){
            perksStr += perksObj[perkColumn].join(" | ")+"\n";
        }
        return perksStr;
    };

};


bot.on('message', function(user, userID, channelID, message, rawEvent) {

    var request = new ArmoryRequest();

    // Check for mentions to see if we're getting a gun request.
    var botId = "<@"+bot.id+">";
    var messageArr = message.split(" ");

    if(botId == messageArr[0]){
        request.init(user, userID, channelID, messageArr, function(result){
            ArmoryBot.postMsg(result, channelID);
        });
    }

});


// Gunsmith Bot
var ArmoryBot = {

    postMsg : function(msg, channelId){

        bot.sendMessage({
            to: channelId,
            message: msg
        });

    }
};

function makeRequest(options, callback){
    request(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            var jsonBody = JSON.parse(body);
            callback(jsonBody);

        }
    });
}
